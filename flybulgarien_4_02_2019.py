import requests
from datetime import datetime
from lxml import html
from tabulate import tabulate

session = requests.session()

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/71.0.3578.98 Safari/537.36',
}

url = ['http://www.flybulgarien.dk/', 'http://apps.penguin.bg/fly/quote3.aspx']


class Scraper:

    """Get, check and parse information about flights.

    """

    def __init__(self, url):
        """The __init__ method.

        Args:
            url (str): Page url.

        """
        self.url = url

    @classmethod
    def check_dates(self, available_dates, date, n):
        """Compares the entered date with available dates.

        Args:
            available_dates (unicode): Available dates for flight.
            date (str): Departure or return date.
            n (int): '0' - first half of available dates list (departure available dates),
                     '1' - second half of available dates list (return available dates).

        """
        try:
            d = datetime.strptime(date, "%d.%m.%Y").strftime("%Y.%m.%d")
        except ValueError:
            print 'Invalid date! Please, try again.'
            input()
        dates = available_dates.replace('[[', '').replace(']]', '').split('-')[n].split('],[')
        dates = [str(x) for x in dates]
        for i in range(len(dates)):
            dates[i] = datetime.strptime(dates[i], "%Y,%m,%d").strftime("%Y.%m.%d")

        today = datetime.strptime(str(datetime.date(datetime.now())), "%Y-%m-%d").strftime("%Y.%m.%d")
        dates = [date for date in dates if date > today]

        if d not in dates:
            print 'No flights for this date: %s' % (d)

            if dates != []:
                print 'Please, try again. Available dates (YYYY.MM.DD): %s' % (dates)
            else:
                print 'No available flights for this airports'
            input()

    def parse(self, depdate, rtdate, aptcode1, aptcode2, passengers):
        """Get information about flight.

        Receive data about flights from the page and save them to 2 lists of Flights class instances(both ways).
        Uses different logic for access to the main page and to the page with flights information.

        Args:
            aptcode1 (str): IATA-code departure airport. For example 'BLL'.
            aptcode2 (str): IATA-code arrival airport. For example 'BOJ'.
            depdate (str): Departure date. For example: '15.07.2019'.
            rtdate (str): Return date. For example: '22.07.2019'.
            passengers: Number of people.

        """
        tree = html.fromstring(self.get(depdate, rtdate, aptcode1, aptcode2, passengers).content)
        if self.url == "http://www.flybulgarien.dk/en/":
            return tree.xpath('//option/text()')[0:6]

        if self.url == "http://apps.penguin.bg/fly/quote3.aspx":
            info = tree.xpath('//td/text()')
            depdate = datetime.strptime(depdate, "%d.%m.%Y").strftime("%d %b %y")
            if rtdate != '':
                rtdate = datetime.strptime(rtdate, "%d.%m.%Y").strftime("%d %b %y")

            going_out = []
            coming_back = []
            for i in range(len(info)):
                if depdate in info[i] and aptcode1 in info[i + 3]:
                    def obj_flight(info):
                        deptime = info[i + 1]
                        rttime = info[i + 2]
                        price = (info[i + 5]).replace('Price:  ', '').replace('EUR', '')
                        aptcode1 = info[i + 3]
                        aptcode2 = info[i + 4]
                        flight_time = datetime.strptime(rttime, '%H:%M') - datetime.strptime(deptime, '%H:%M')

                        flight = Flight(aptcode1=aptcode1, aptcode2=aptcode2, date=info[i], deptime=deptime,
                                        rttime=rttime, class_=None, price=price, flight_time=flight_time)
                        return flight

                    if rtdate == '':
                        print obj_flight(info).__str__()
                    going_out.append(obj_flight(info))

                if rtdate != '':
                    if rtdate in info[i] and aptcode2 in info[i + 3]:
                        coming_back.append(obj_flight(info))

            print Path(going_out=going_out, coming_back=coming_back, total_price=[]).__str__()

    def get(self, depdate, rtdate, aptcode1, aptcode2, passengers):
        """Send request (get method) to the server and get response.

        Uses different logic for access to the main page and to the page with flights information.

        Returns:
            Response with information about available airports(from main page) or flights.

        """

        if self.url == 'http://www.flybulgarien.dk/en/':
            params = {}

        if self.url == 'http://apps.penguin.bg/fly/quote3.aspx':
            params = {
                'lang': 'en',
                'depdate': depdate,
                'aptcode1': aptcode1,
                'aptcode2': aptcode2,
                'paxcount': passengers,
                'infcount': ''
            }
            params.update({'ow': ''} if rtdate == '' else {'rt': '', 'rtdate': rtdate})

        resp = session.get(self.url, headers=headers, params=params)
        return resp

    def post(self, aptcode1, aptcode2):
        """Send request (post method) to the server and get response.

        Returns:
            Response with information about available dates.

        """

        data = {
            'code1': aptcode1,
            'code2': aptcode2,
        }

        resp = session.post(self.url, headers=headers, data=data)
        return resp.text


class Flight:

    """Contains and displays information about existing flight.

    """

    def __init__(self, aptcode1, aptcode2, date, deptime, rttime, class_, price, flight_time):
        """The __init__ method.

        Args:
            aptcode1 (unicode): IATA-code departure airport.
            aptcode2 (unicode): IATA-code arrival airport.
            date (unicode): Return or departure date.
            deptime (unicode): Departure time.
            rttime (unicode): Return time.
            class_ (None): Class (economy/business).
            price (unicode): Ticket price.
            flight_time (datetime.timedelta): Flight time.

        """
        self.aptcode1 = aptcode1
        self.aptcode2 = aptcode2
        self.date = date
        self.deptime = deptime
        self.rttime = rttime
        self.class_ = class_
        self.price = price  
        self.flight_time = flight_time

    def __str__(self):
        """Displays information about flight in column.

        Returns:
            Information about flight in column.

        """
        lst = ['From city: %s' % (self.aptcode1), 'To city: %s' % (self.aptcode2),
               'Date: %s' % (self.date), 'Departure time: %s' % (self.deptime), 'Arrival time: %s' % (self.rttime),
               'Class: %s' % (self.class_), 'Price: %s' % (self.price), 'Flight time: %s' % (self.flight_time)]
        return '\n'.join(lst)

class Path:

    """Contains and displays all possible flights (both ways).

    """

    def __init__(self, going_out, coming_back, total_price):
        """The __init__ method.

        Args:
            going_out (list): List of departure flights.
            coming_back (list): List of return flights.
            total_price (float): Sum of departure and return flight price.

        """
        self.going_out = going_out
        self.coming_back = coming_back
        self.total_price = total_price

    def __str__(self):
        """Makes different combinations of all available flights in both ways.

        Returns:
                    Combinations of all available flights in both ways.

        """

        path = []

        for i in range(len(self.going_out)):
            for j in range(len(self.coming_back)):
                total_price = float(self.going_out[i].price) + float(self.coming_back[j].price)
                path.append(
                    [self.going_out[i].__str__(), self.coming_back[j].__str__(), 'Total price: %s EUR' % (total_price)])

        return tabulate(path)

def input():
    """Input function.

    Get and validate the entered data. If the check fails, it goes to the beginning.

    """
    aptcode1 = raw_input('From city: ')
    aptcode2 = raw_input('To city: ')

    cities = Scraper(url=url[0] + 'en/').parse(0, 0, aptcode1, aptcode2, 0)

    while any(aptcode1 and aptcode2 in city for city in cities):
        depdate = raw_input('Departure Date (DD.MM.YYYY): ')
        rtdate = raw_input('Return Date (DD.MM.YYYY): ')
        passengers = raw_input('Passengers: ')

        try:
            int(passengers)
        except:
            print "Invalid data in 'Passengers'"

        available_dates = Scraper(url=url[0] + 'script/getdates/2-departure').post(aptcode1, aptcode2)

        if available_dates == '[]':
            print 'No available flights for this airports. Please, enter another airports'
            input()
        else:
            if rtdate == '':  # One-way
                Scraper.check_dates(available_dates, depdate, 0)

            else:  # Return
                Scraper.check_dates(available_dates, depdate, 0)
                Scraper.check_dates(available_dates, rtdate, 1)

            Scraper(url=url[1]).parse(depdate, rtdate, aptcode1, aptcode2, passengers)
            print 'Do you need to find more flights?'
            input()
    else:
        print 'Please, try again. Airport is not available!'
        input()


if __name__ == "__main__":
    # This will only be executed when this module is run.
    input()




