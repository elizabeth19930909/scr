"""This scraper searches for information about available flights
on the website http://www.flybulgarien.dk/en/ (company "Fly Bulgarien").

You can run the scraper from the command line: python.exe flybulgarien.py. You
will be asked the details of your outbound and inbound flights: names of the
airports (IATA-code), dates and number of passengers.

After that, you will receive the flight information which consists of: airports
name, date, departure and arrival time, total price and flight duration.

For the test purposes, you can use following parameters:

From city: 'BLL'
To city: 'BOJ'
Departure date: '15.07.2019'
Return date: '22.07.2019'
Passengers: '1'

It can search for both round trips and one way tickets.
In the second case, instead of the return date, press Enter.

"""

from datetime import datetime
import requests
from lxml import html
from tabulate import tabulate

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) '
                  'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 '
                  'Safari/537.36',
}

URL = ['http://www.flybulgarien.dk/', 'http://apps.penguin.bg/fly/quote3.aspx']

session = requests.session()


class Scraper:

    """Get, check and parse the flight information.

    """

    def __init__(self, url):
        """The __init__ method.

        Args:
            url (str): Page url.

        """
        self.url = url

    @classmethod
    def check_dates(cls, available_dates, date, number):
        """Compare the entered date with the available dates.

        Args:
            available_dates (str): Available dates for flight.
            date (str): Departure or return date.
            number (int): '0' - departure available dates list
                          '1' - return available dates list

        """
        try:
            date = datetime.strptime(date, "%d.%m.%Y").strftime("%Y.%m.%d")
        except ValueError:
            print "Invalid data in 'Departure Date/Return Date'! " \
                  "Please, try again."
            main()

        dates = available_dates.replace('[[', '').replace(']]', '')
        dates = dates.split('-')[number].split('],[')
        dates = [str(x) for x in dates]

        for i in range(len(dates)):
            dates[i] = datetime.strptime(dates[i], "%Y,%m,%d")
            dates[i] = dates[i].strftime("%Y.%m.%d")
        today = str(datetime.date(datetime.now()))
        today = datetime.strptime(today, "%Y-%m-%d").strftime("%Y.%m.%d")
        dates = [elem for elem in dates if elem > today]

        if date not in dates:
            print 'No flights for this date: %s' % date

            if dates:
                print 'Please, try again. ' \
                      'Available dates (YYYY.MM.DD): %s' % dates
            else:
                print 'No available flights for this airports'
            main()

    def parse_airports(self):
        """Parse information about available airports.

        Select information about available airports.

        """
        get = self.get_airports().content
        tree = html.fromstring(get)
        return tree.xpath('//option/text()')[0:6]

    def parse_flights(self, depdate, rtdate, aptcode1, aptcode2, passengers):
        """Parse information about flights.

        Select information about available flights and save them to the 2 lists
        of the Flights class instances(both ways).

        Args:
            aptcode1 (str): IATA-code departure airport.
            aptcode2 (str): IATA-code arrival airport.
            depdate (str): Departure date.
            rtdate (str): Return date.
            passengers (str): Number of people.

        """
        get = self.get_flights(depdate, rtdate, aptcode1, aptcode2,
                               passengers).content
        tree = html.fromstring(get)
        info = tree.xpath('//td/text()')
        depdate = datetime.strptime(depdate, "%d.%m.%Y")
        depdate = depdate.strftime("%d %b %y").replace('0', '')
        if rtdate:
            rtdate = datetime.strptime(rtdate, "%d.%m.%Y")
            rtdate = rtdate.strftime("%d %b %y").replace('0', '')

        going_out = []
        coming_back = []
        for i in range(len(info)):
            def obj_flight(info):
                """Extract information about flight and save them as the Flight
                class instance.

                Args:
                    info (str): Information about all flights.

                Returns:
                    The Flight class instance.

                """
                date = info[i]
                deptime = info[i + 1]
                rttime = info[i + 2]
                price = info[i + 5]
                price = price.replace('Price:  ', '').replace('EUR', '')
                aptcode1 = info[i + 3]
                aptcode2 = info[i + 4]
                flight_time = str(datetime.strptime(rttime, '%H:%M')
                                  - datetime.strptime(deptime, '%H:%M'))

                flight = Flight(aptcode1=aptcode1, aptcode2=aptcode2,
                                date=date, deptime=deptime, rttime=rttime,
                                class_=None, price=price,
                                flight_time=flight_time)
                return flight

            if depdate in info[i] and aptcode1 in info[i + 3]:
                going_out.append(obj_flight(info))
                if not rtdate:
                    print obj_flight(info).__str__()

            if rtdate:
                if rtdate in info[i] and aptcode2 in info[i + 3]:
                    coming_back.append(obj_flight(info))

        path = Path(going_out=going_out, coming_back=coming_back,
                    total_price=0)
        return path.print_path()

    def get_airports(self):
        """Send request (get method) to the server and get response.

        Send request to the main page and get response with information about
        available airports.

        Returns:
            Response with information about the available airports.

        """
        resp = session.get(self.url, headers=headers)
        return resp

    def get_flights(self, depdate, rtdate, aptcode1, aptcode2, passengers):
        """Send request (get method) to the server and get response.

        Send request and get response with information about available flights.

        Returns:
            Response with information about the available flights.

        """
        params = {
            'lang': 'en',
            'depdate': depdate,
            'aptcode1': aptcode1,
            'aptcode2': aptcode2,
            'paxcount': passengers,
            'infcount': ''
        }
        params.update({'ow': ''} if rtdate == ''
                      else {'rt': '', 'rtdate': rtdate})

        resp = session.get(self.url, headers=headers, params=params)
        return resp

    def post(self, aptcode1, aptcode2):
        """Send request (post method) to the server and get response.

        Returns:
            Response with information about available dates.

        """

        data = {
            'code1': aptcode1,
            'code2': aptcode2,
        }

        resp = session.post(self.url, headers=headers, data=data)
        return resp.text


class Flight:

    """Contains and displays information about existing flight.

    """

    def __init__(self, aptcode1, aptcode2, date, deptime, rttime, class_,
                 price, flight_time):
        """The __init__ method.

        Args:
            aptcode1 (str): IATA-code departure airport.
            aptcode2 (str): IATA-code arrival airport.
            date (str): Return or departure date.
            deptime (str): Departure time.
            rttime (str): Return time.
            class_ (None): Class (economy/business).
            price (str): Ticket price.
            flight_time (str): Flight duration.

        """
        self.aptcode1 = aptcode1
        self.aptcode2 = aptcode2
        self.date = date
        self.deptime = deptime
        self.rttime = rttime
        self.class_ = class_
        self.price = price
        self.flight_time = flight_time

    def __str__(self):
        """Displays the flight information in the column.

        Returns:
            The flight information in the column.

        """
        lst = ['From city: %s' % self.aptcode1, 'To city: %s' % self.aptcode2,
               'Date: %s' % self.date, 'Departure time: %s' % self.deptime,
               'Arrival time: %s' % self.rttime, 'Class: %s' % self.class_,
               'Price: %s' % self.price, 'Flight time: %s' % self.flight_time]
        return '\n'.join(lst)


class Path:

    """Contains and displays all the possible flights (both ways).

    """

    def __init__(self, going_out, coming_back, total_price):
        """The __init__ method.

        Args:
            going_out (list): Departure flights list.
            coming_back (list): Return flights list.
            total_price (float): Sum of the departure and return flight price.

        """
        self.going_out = going_out
        self.coming_back = coming_back
        self.total_price = total_price

    def __str__(self):
        """Makes different combinations of the available flights in the both
        ways.

        Returns:
            Combinations of the available flights in the both ways.

        """
        path = []
        for i in range(len(self.going_out)):
            for j in range(len(self.coming_back)):
                self.total_price = float(self.going_out[i].price) +\
                                   float(self.coming_back[j].price)
                path.append(
                    [self.going_out[i].__str__(),
                     self.coming_back[j].__str__(),
                     'Total price: %s EUR per person' % self.total_price])
        return tabulate(path)

    def print_path(self):
        """Print all the available flights in the both ways.

        """
        print self.__str__()


def main():
    """Input function.

    Get and validate the entered data. If the check fails, would be directed
    to the beginning.

    """
    aptcode1 = raw_input('From city: ')
    aptcode2 = raw_input('To city: ')
    cities = Scraper(url=URL[0] + 'en/').parse_airports()
    while any(aptcode1 and aptcode2 in city for city in cities):
        depdate = raw_input('Departure Date (DD.MM.YYYY): ')
        rtdate = raw_input('Return Date (DD.MM.YYYY): ')
        passengers = raw_input('Passengers: ')
        try:
            int(passengers)
            if int(passengers) > 8:
                print 'The maximum number of passengers is 8!'
                continue
        except ValueError:
            print "Invalid data in 'Passengers'!"
            continue

        url_for_dates = 'script/getdates/2-departure'
        available_dates = Scraper(url=URL[0] + url_for_dates).post(aptcode1,
                                                                   aptcode2)
        if available_dates == '[]':
            print 'No available flights for this airports. ' \
                  'Please, enter another airports.'
            main()
        else:
            if not rtdate:  # One-way
                Scraper.check_dates(available_dates, depdate, 0)

            else:  # Return
                Scraper.check_dates(available_dates, depdate, 0)
                Scraper.check_dates(available_dates, rtdate, 1)

            Scraper(url=URL[1]).parse_flights(depdate, rtdate, aptcode1,
                                              aptcode2, passengers)
            print 'Do you need to find more flights?'
            main()
    print 'Please, try again. Airport is not available!'
    main()


if __name__ == "__main__":
    # This will only be executed when this module is run.
    main()
