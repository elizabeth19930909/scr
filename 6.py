# coding=utf-8

import requests
import re

session = requests.session()
# 1 GET http://fo-emea.ttinteractive.com/Zenith/FrontOffice/Ewa/en-GB/?mode=iframe

headers = {
'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
}

params = {
'mode':'iframe'
}
resp = session.get("http://fo-emea.ttinteractive.com/Zenith/FrontOffice/Ewa/en-GB/", params=params, headers=headers)
print session.cookies
url = resp.url
print url
TempDataGuid = resp.url.split('(')[2].split(')')[0]
print TempDataGuid

tmp = resp.text
#print tmp

# airports_info = re.compile('"Airports":(.*)"Countries"').findall(tmp)[0][:-1] # извлечь DataId, Code, Label
cnv = re.compile('__cnv=[^".]*').findall(tmp)[0].replace('__cnv=','')
print cnv
#print airports_info


# 2  http://fo-emea.ttinteractive.com/Zenith/FrontOffice/(S(05crouy2um0255zkvkdakhla))/Ewa/en-GB/BookingEngine/SearchFlights

body = {
'SaleConditionAccepted':'false',
'ExtendedSearchDayCount':'3',
'DoNotCheckCache':'false',
'AlreadyLoggedIn':'false',
'TempDataGuid':TempDataGuid,
'CurrencyCode':'EUR',
'FareBasisDataId':'',
'Travelers[0][DataId]':'1',
'Travelers[0][Count]':'1',
'UserSelections[0][SelectedDate]':'2019-01-25T00:00:00',
'UserSelections[0][IsOpen]':'false',
'UserSelections[0][ReferenceDate]':'2019-01-25T00:00:00',
'UserSelections[0][DataIdOrigin]':'5321',
'UserSelections[0][DataIdDestination]':'6953',
'UserSelections[0][GenericClassDataId]':'',
'UserSelections[0][SelectedSegments]':'',
'JsonPrepareBookingRequest':'',
'IsFFPRewardSearch':'false',
'IsTCSearch':'false',
'PromoCode':'',
'IsNewBooking':'true',
'ShowingWLWarningMessageActivated':'false'
}

params = {
    '__cnv':str(cnv)
}

# cookies = dict(cookies_are = '__RequestVerificationToken_L1plbml0aC9Gcm9udE9mZmljZQ2=l9VpVAKYQ68haneu9avhmrom3bxNrgECTbRRVlphaIrTLOHPxi32QQUMzXy_YNA7VIZikmgzkVV1RNxiZ__4x5TZtqE1OOooefxzXaAnx4Q1')
resp = session.post("http://fo-emea.ttinteractive.com/Zenith/FrontOffice/(S("+TempDataGuid+"))/Ewa/en-GB/FlexibleFlightStaticAjax/FlexibleFlightListLoadSelectedDays", data=body, params=params, headers=headers)

print resp.text

